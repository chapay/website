---
title: "Hello, World!"
date: 2022-01-12T23:34:26+01:00
toc: false
tags:
  - blog
---

Hi everyone!

I wanted to start writing for a long time already and finally found some time and courage for it. It’s nice to share something with other people and I'd like to create content, not just consume.

I have recently found [**#100DaysToOffload**](https://100daystooffload.com/) challenge and like the idea behind it, but 100 posts per year is too much for me. Instead I will try to write a post per week, so for me it will be **#oneWeekOnePost**.

My posts will be in English (even though it’s not my native language) to reach a bigger auditory and to practice my writing skills.

My blog is powered by [Hugo](https://gohugo.io/) with [hello-friend-ng](https://themes.gohugo.io/themes/hugo-theme-hello-friend-ng/) theme. I'm going to write separate posts about how to set it up and deploy on AWS:

- [How to setup Hugo to render your blog](/tech/how-to-set-up-hugo-to-render-your-blog/)
- How to host static website with AWS S3 and CloudFront
- How to build and deploy your blog automatically on every commit

You can subscribe either to [combined RSS](/index.xml) or separately to [Tech](/tech/index.xml), [Hobby](/hobby/index.xml) or [Life](/life/index.xml) sections.

Git repository with the source code is hosted on [Codeberg](https://codeberg.org/chapay/website).

I hope you will find my blog interesting!
