---
title: "About"
date: 2022-01-12T23:21:06+01:00
toc: false
---

Hi, I'm Andrey.

I work as Software Engineer and have a master's degree in Computer Science.

My favorite topics are privacy, self-hosting and [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software).

I also love boardgames, Lego and old video games :)

You can subscribe to my blog using [combined RSS](/index.xml) or separate [Tech](/tech/index.xml), [Hobby](/hobby/index.xml) or [Life](/life/index.xml) sections.

Source code can be found on [Codeberg](https://codeberg.org/chapay/website).

I hope you will enjoy my blog.

Have a nice day!
