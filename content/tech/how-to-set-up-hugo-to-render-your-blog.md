---
title: "How to setup Hugo to render your blog"
date: 2022-01-23T19:36:08+01:00
toc: false
tags:
  - blog
  - hugo
---

In this post I will tell you why I chose [Hugo](http://gohugo.io/) and how I customized it.

## Why actually Hugo?
For blog there are several popular options:
- social network (e.g. Mastodon/Twitter/Reddit/Telegram)
- 3rd party blogging service (e.g. Medium)
- blog engine with WYSIWYG editor (e.g. Wordpress)
- static site generator (e.g. Jekyll/Hugo)

I have several important requirements for my blog: it should be able to self-host, work without javascript and have no tracking or ads.

So 3rd party options are no go for me and I prefer to leave social networks for short posts and [backfeeding](https://indieweb.org/backfeed).
Though I find blog engines with WYSIWYG editor the most convenient to use, I also find them the most difficult to host since you have to care about its performance, security and other things which come together.
I want to easily self-host my blog or use any CDN for it, so [static site generator](https://www.cloudflare.com/learning/performance/static-site-generator/) seems the best option for me.

[Hugo](https://gohugo.io/) caught my attention with its fast-developing community and I gave it a try.

## Initial setup
First of all we need to create some boilerplate, for this you can check [Quickstart](https://gohugo.io/getting-started/quick-start/) page from official documentation.
Basically you have to:
- install Hugo on your local machine
- choose a [theme](https://themes.gohugo.io) (I chose [hello-friend-ng](https://themes.gohugo.io/themes/hugo-theme-hello-friend-ng/)) and install it
- (optional) [install plugins](https://gohugo.io/tools/editors/) for your IDE
- modify config file to fill in your data
- create a test post

## Customization
I wanted to change some things on my blog, so I had to make additional customization. For this you usually just override default and theme [partials](https://gohugo.io/templates/partials/) and css with your own.

### RSS feed
First of all I created a page `About` and sections for posts: `Tech`, `Hobby` and `Life` . I wanted to have separate RSS feeds for all of them (excluding `About` page) plus one combined for all posts.

To create a combined RSS I put the content of [default template](https://github.com/gohugoio/hugo/blob/master/tpl/tplimpl/embedded/templates/_default/rss.xml) to `layouts/_default/index.rss.html` and replaced
```text
{{ range $pages }}
```
with
```text
{{ range where (where .Site.Pages ".Section" "not in" "about") "Kind" "page" }}
```
We exclude `about` section here and add all posts excluding sections themselves.

Next step was to change RSS to contain the whole content, not just summary, so reader didn’t have to go to my blog to read full post.

For that I copied default template to `layouts/_default/rss.html` (template for one section RSS feed) and replaced
```text
{{ .Summary | html }}
```
with
```text
{{- .Content | html -}}
```
in both `index.rss.html` and `rss.html` templates.

### Custom section titles
Hugo is too smart and tries to guess a title for the section, so for `Tech` section the title will be `Techs` which makes no sense. To override it I had to create `tech/_index.md` file and specify the correct title:
```text
---
title: "Tech"
---
```

### Other customizations
I also had to customize other small things which I’ll just mention:
- customize the footer to have a correct link to RSS
- enable [git support](https://gohugo.io/variables/git/) to have links to my repository and display last update date automatically
- add custom css file to make font smaller and change how footer looks like on mobile
- add additional icon for [Codeberg](https://codeberg.org)
- remove PrismJS and use server solution for syntax highlighting instead (Chroma which comes by default with Hugo)

### Bonus: verify your blog on Mastodon
I wanted to have a verification sign next to my website link in [my Mastodon profile](https://fosstodon.org/@chapay), so I followed an instruction and added `rel=“me”` to the link to Mastodon:
```
<a rel="me" href="https://fosstodon.org/@chapay">Mastodon</a>
```
and then saved settings for my profile again to let Mastodon check my blog.

## Conclusion

It doesn't take a lot of time to setup Hugo for your blog, but in the end you will get a highly customizable and easy to self-host static website. In the next post I will explain how to host it on AWS.
